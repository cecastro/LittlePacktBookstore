﻿using LittlePacktBookstore.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittlePacktBookstore.Data {
    public class LittlePacktBookStoreDbContext:DbContext {
        public LittlePacktBookStoreDbContext(DbContextOptions options):base(options) { }
        public DbSet<Book> Books { get; set; }
        public DbSet<Carousel> Carousels { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}
